﻿import "jasmine";
import { MainState } from "../../../../src/app/store/main";
import { MAIN_INITIAL_STATE } from "../../../../src/app/store/main/main.state";



describe("main reducer", ()=> {
    it("should handle initial state", ()=>{
        const mainInitialState: MainState = MAIN_INITIAL_STATE;
        expect(mainInitialState).toEqual({});
    });
    
}); 