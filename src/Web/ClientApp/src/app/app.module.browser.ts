import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppModuleShared } from './app.module.shared';
import { MainComponent } from './components/main/main.component';
import { SuiModule, SuiDimmerModule, SuiModalModule, SuiTabsModule } from 'ng2-semantic-ui';
import { AuthComponent } from './components/auth/auth.component';



@NgModule({
    bootstrap: [ MainComponent],
    imports: [
        BrowserModule,
        AppModuleShared,
        BrowserAnimationsModule,
        
    ],
    providers: [
        { provide: 'BASE_URL', useFactory: getBaseUrl },       
    ],
    declarations:[ ]
})
export class AppModule {
}

export function getBaseUrl() {
    return document.getElementsByTagName('base')[0].href;
}
