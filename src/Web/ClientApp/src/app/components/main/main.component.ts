import { OnInit, AfterViewInit } from '@angular/core';
import { Component, PLATFORM_ID, OnDestroy} from '@angular/core';
import { AppState } from '../../../store/app.state';
import { Store } from '@ngrx/store';
import { HidePrebootAction } from '../../store/main/main.actions';
import { isPlatformServer } from '@angular/common';




@Component({
    selector: 'app',
    templateUrl: './main.component.html',
    styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit, AfterViewInit, OnDestroy {
   

    constructor(private store:Store<AppState>){}

    ngOnInit(): void {        
        
    }
    ngAfterViewInit(): void {   
        setTimeout(()=>{
            this.store.dispatch(new HidePrebootAction(true) );
        },1250)
        
    }
    onActivate(){ 
        if(!isPlatformServer(PLATFORM_ID )) {

        }
        if(typeof window != 'undefined'){
                    
        }  
        window.scrollTo(0,0);    
        
    }

    ngOnDestroy(): void {
        this.store.dispatch(new HidePrebootAction(false) );
    }
    // getValueFromObservable<T>(obs:Observable<T | null>):T | null{
    //     if(obs){
    //         let subject = new BehaviorSubject(null);
    //         const subscription = obs.subscribe(subject);
    //         let data:T | null = subject.getValue();
    //         subscription.unsubscribe();
    //         return data;
    //     }
    //     return null;
        
    // }
}
