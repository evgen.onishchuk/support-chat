import { Observable } from 'rxjs/Observable';
import { Component, HostListener, ElementRef, ViewChild, EventEmitter, Output, OnInit, Input } from "@angular/core";
import { Interval } from "../../models/interval.enum";
import { Store } from "@ngrx/store";
import { AppState } from "../../../store/app.state";
import { ShowSignUpAction, AuthActions } from '../../store/auth/auth.actions';
import { Actions } from '@ngrx/effects';


@Component({
    selector: "app-nav",
    templateUrl: "./nav.component.html",
    styleUrls: [ "./nav.component.css" ]
})
export class NavComponent implements OnInit{
 

    @ViewChild('header')
    header:ElementRef;

    @Output("scrollToTop")
    scrollToTop:EventEmitter<void> = new EventEmitter<void>();

    @Output("scrollTo")
    scrollTo:EventEmitter<string> = new EventEmitter<string>();

    @Output('setActive')
    setActiveSection:EventEmitter<Interval> = new EventEmitter<Interval>();

    @Input('activeInterval')
    activeInterval:Interval = Interval.None;

    @Input('invertHeader')
    invertHeader: boolean = true;

    @Input("mainHeader")
    mainHeader: boolean = false;

    showPopup: boolean = false;

    public get offset():number {       
        return this.header.nativeElement.clientHeight+1;
    }

    modalOpened:Observable<boolean>;

    constructor(private store:Store<AppState>, private actions$: Actions){}

    ngOnInit(): void {     
        this.modalOpened = this.actions$.ofType(AuthActions.ShowSignUp)
            .map( (action: ShowSignUpAction) => !!action.payload)   
    }

    @HostListener("body:click", ['$event'])
    onBodyClick(event:any){       
        this.showPopup = false;
    }

    isActive(value: number):boolean{
        return value == this.activeInterval;
    }

    setActive(value: number):void{
        this.setActiveSection.emit(value);
    }
    showAuth(authType: string): void {        
        this.store.dispatch(new ShowSignUpAction(authType));        
    }

    isAuthorized(): boolean {
        return false;
    }

    onClick(): void {
        this.showPopup = !this.showPopup;
    }
}