import { Component } from "@angular/core";


@Component({
    selector: "pricing",
    templateUrl: "./pricing.component.html",
    styleUrls: [ "./pricing.component.css" ]
})
export class PricingComponent {
    tooltip:string = "tooltip";
}