import { trigger, state, style, animate, transition } from '@angular/animations';
import { Component, OnInit, AfterViewInit, ElementRef, ViewChild, ComponentFactoryResolver } from "@angular/core";
import * as validator from "card-validator";
import { AppState } from "../../../../store/app.state";
import { Store } from "@ngrx/store";
import { /*StripeLoadedAction*/ MainActions } from "../../../store/main/main.actions";
import { StripeService } from "../../../services/stripe.service";
import { CardNumberDirective } from "../../../directives/card-number.directive";
import { Actions } from "@ngrx/effects";
import { FormGroup } from "@angular/forms";

declare let Stripe: any;

@Component({
    selector: "card",
    templateUrl: "./card.component.html",
    styleUrls: ["./card.component.css"],
    animations: [ 
        trigger("flip",[
            state("fliped",style({
                transform: 'rotateY(180deg)'
            })),            
            transition("normal => fliped", animate('500ms ease-out')),
            transition("fliped => normal", animate('500ms ease-in'))
        ])
    ]
})
export class CardComponent implements OnInit, AfterViewInit{
    
    @ViewChild("cardNumber")
    cardNumber:ElementRef;

    showBack: boolean = false;
    stripe: any;
    cardNumberElement: any; 
    focus:boolean = false;
    stripeLoaded: boolean = false;
    hidePlaceholder: boolean = false;

    form:FormGroup = new FormGroup({});

    constructor(private store:Store<AppState>, 
        private stripeService:StripeService, 
        private componentFactoryResolver: ComponentFactoryResolver,
        private actions$:Actions
    ){}

    ngOnInit(): void {
        this.actions$.ofType(MainActions.StripeLoaded)
        .subscribe( _ => {
            this.stripeLoaded = true;
            console.log()
        })
        console.log("validator", validator);
        // 
        
        //         let elements = this.stripe.elements();
        //         this.cardNumberElement = elements.create('cardNumber');
        //         this.cardNumberElement.on('focus', ()=> {                    
        //             this.focus = true;
        //         });
        //         this.cardNumberElement.on('blur', ()=> {                    
        //             this.focus = false;
        //         });
                // setTimeout(()=>{
                //     this.store.dispatch( new StripeLoadedAction());
                //     console.log("stripe@@@@");
                // },2000);
                // console.log("stripe", this.card);     
    }

    ngAfterViewInit(): void {   
        // this.stripeService.cardNumberElement.on('focus',()=> {
        //     this.focus = true;
        // });    
        // this.stripeService.cardNumberElement.on('blur',()=> {
        //     this.focus = false;
        // });    
        // this.stripeService.cardNumberElement.on('change',(result: any)=> {   
        //     // console.log("card", result);
        //     this.hidePlaceholder = !result.empty;
           
        // }); 
        // this.stripeService.cardNumberElement.mount(this.cardNumber.nativeElement);
        // console.log()
    //    this.cardNumberElement.
        // let viewContainerRef = this.cardNumber.viewContainerRef;
        // viewContainerRef.clear();
        // let componentFactory = this.componentFactoryResolver.resolveComponentFactory()
        // viewContainerRef.createComponent(componentFactory)
        // this.cardNumber.appendComponent(this.stripeService.cardNumberElement);
    }

    onFocus(): void {       
        this.showBack = true;
    }

    onBlur(): void {  
        this.showBack = false;
    }
    onCardNumberBlur(value: string): void {
        
        console.log("card value", value);
        console.log("result", validator.number(value));
    }
    onInput(target:any) {
        console.log("input",validator.number(target));
        console.log("input",validator.cvv("255"));
    }
}