import { Component } from "@angular/core";


@Component({
    selector: "plans",
    templateUrl: "./plans.component.html",
    styleUrls: ["./plans.component.css"]
})
export class PlansComponent {

    selectedPlan:number = 1;
    monthly: boolean = true;
}