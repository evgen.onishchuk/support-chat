import { Component, OnInit, ElementRef, ViewChild, AfterViewInit } from "@angular/core";
import { Store } from "@ngrx/store";
import { AppState } from "../../../store/app.state";
import { MainActions } from "../../store/main/main.actions";
import { Actions } from "@ngrx/effects";
import { StripeService } from "../../services/stripe.service";
import * as $ from "jquery";

@Component({
    selector: "subscription",
    templateUrl: "./subscription.component.html",
    styleUrls: [ "./subscription.component.css" ]
})
export class SubscriptionComponent implements OnInit, AfterViewInit{
    

    activeStep: number = 1;

    stripeLoaded: boolean = false;    

    constructor(private store:Store<AppState>, private stripeService:StripeService){}
    
    ngOnInit(): void {   
        
    }

    ngAfterViewInit(): void {
       
    }


    setActive(activeStep:number):void {
        this.activeStep = activeStep;
    }

    nextStep():void {
        this.activeStep++;
    }
    previousStep(): void {
        this.activeStep--;
    } 
}