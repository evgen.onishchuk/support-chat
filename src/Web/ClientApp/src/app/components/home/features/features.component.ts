import { Component, ViewChild, ElementRef } from "@angular/core"


@Component({
    selector: 'features',
    templateUrl: "./features.component.html",
    styleUrls: ["./features.component.css"]
})
export class FeaturesComponent {

    @ViewChild('features')
    features:ElementRef;


    get nativeElement(): HTMLElement {
        return this.features.nativeElement;
    }

}