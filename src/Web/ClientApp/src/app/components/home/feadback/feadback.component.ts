import { Component, ElementRef, ViewChild } from "@angular/core";
import { FormGroup } from "@angular/forms";


@Component({
    selector: "feadback",
    templateUrl: "./feadback.component.html",
    styleUrls: ['./feadback.component.css']
})
export class FeadbackComponent {

    @ViewChild('feadback')
    feadback: ElementRef;

    form: FormGroup = new FormGroup({});

    get nativeElement(): HTMLElement {
        return this.feadback.nativeElement;
    }

}