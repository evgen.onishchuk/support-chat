import { Component, ViewChild, ElementRef } from '@angular/core';


@Component({
    selector: "subscription-plans",
    templateUrl: "./plans.component.html",
    styleUrls: ["./plans.component.css"]    
})
export class SubscriptionPlansComponent {

    @ViewChild('pricing')
    pricing:ElementRef;

    tooltip:string = "tooltip";

    get nativeElement(): HTMLElement {
        return this.pricing.nativeElement;
    }
    
}