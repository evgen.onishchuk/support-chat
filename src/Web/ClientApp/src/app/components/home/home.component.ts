import { Component, ElementRef, EventEmitter, ViewChild, HostListener, AfterViewInit } from "@angular/core";
import { PageScrollInstance, PageScrollService } from "ng2-page-scroll";
import { Interval } from "../../models/interval.enum";
import { NavComponent } from "../nav/nav.component";
import { SubscriptionPlansComponent } from './plans/plans.component';
import { FeaturesComponent } from './features/features.component';
import { FeadbackComponent } from './feadback/feadback.component';




@Component({
    selector:"home",
    templateUrl: "./home.component.html",
    styleUrls: [ "./home.component.css" ]    
})
export class HomeComponent implements AfterViewInit{  

    @ViewChild(NavComponent)
    navComponent: NavComponent;

    @ViewChild('firstElement')
    firstItem:ElementRef;

    @ViewChild(FeaturesComponent)
    features: FeaturesComponent

    @ViewChild(SubscriptionPlansComponent)
    pricing:SubscriptionPlansComponent

    @ViewChild(FeadbackComponent)
    feadback:FeadbackComponent
   

    offset:number = 0;

    scrollClicked: boolean | undefined= false;

    featuresInterval:[number,number] = [0,0];

    pricingInterval:[number,number] = [0,0];

    feedbackInterval: [number,number] = [0,0];
    
    activeInterval:Interval = Interval.None;

    invertHeader: boolean  = false;    

    scrollFinished:  EventEmitter<boolean> = new EventEmitter<boolean>();


    constructor(private pageScrollService: PageScrollService){}
    
    ngOnInit(): void {
        this.scrollFinished.subscribe((_: boolean) => {
            this.scrollClicked = undefined;
        });     
        
    } 

        ngAfterViewInit(): void {              
            this.initOffsetIntervals();
            // 
           
        }

        @HostListener("window:scroll", ['$event'])
        onWindowScroll(event:any):void {       
            const value: number =  -document.body.getBoundingClientRect().top;         
            if (this.scrollClicked == undefined) {
                this.scrollClicked = false;
            } else if (this.scrollClicked == false) {
                if (this.isInInterval(Interval.Features, value)) {
                    this.activeInterval = Interval.Features;
                } else if (this.isInInterval(Interval.Pricing, value)) {
                    this.activeInterval = Interval.Pricing;
                } else if (this.isInInterval(Interval.Feedback, value)) {
                    this.activeInterval = Interval.Feedback;
                } else {
                    this.activeInterval = Interval.None;
                }
            }
            this.invertHeader = Math.abs(value) !== 0;
        }
        
        scrollToTop(): void{
            this.scrollTo("body");
            this.activeInterval = Interval.None;
        }
        scrollTo(selector:string): void {             
            const pageScrollInstance:PageScrollInstance 
                = PageScrollInstance.newInstance(
                    {
                        document:document,
                        scrollTarget:selector,
                        pageScrollOffset:this.offset,
                        pageScrollFinishListener: this.scrollFinished,
                        pageScrollDuration:400
                    }
                );
            this.scrollClicked = true;
            this.pageScrollService.start(pageScrollInstance);
        }
        
        setActive(value: Interval):void{
            this.activeInterval = value;
        }

        private isInInterval(interval: Interval, value:number): boolean {
            switch (interval) {
                case Interval.Features:
                    return value >= this.featuresInterval[0] && value <= this.featuresInterval[1];
                case Interval.Pricing:
                    return value >= this.pricingInterval[0] && value <= this.pricingInterval[1];
                case Interval.Feedback:
                    return value >= this.feedbackInterval[0] && value <= this.feedbackInterval[1];
                default:
                    return false;
            }
        }
        
        private initOffsetIntervals(): void {       
            this.offset = this.navComponent.offset;           
            let sectionOffset = this.offset + 100;
    
            let currentOffset: number = this.firstItem.nativeElement.clientHeight;
    
            let featuresStart: number  = currentOffset  - sectionOffset;
            currentOffset += this.features.nativeElement.clientHeight;
            let featuresEnd: number = currentOffset - sectionOffset;
            this.featuresInterval = [featuresStart, featuresEnd];
    
            let pricingStart = currentOffset - sectionOffset;
            currentOffset += this.pricing.nativeElement.clientHeight
            let pricingEnd = currentOffset  - sectionOffset;
            this.pricingInterval = [pricingStart, pricingEnd];
    
            let feedbackStart: number = currentOffset - sectionOffset;
            currentOffset += this.feadback.nativeElement.clientHeight;
            let feedbackEnd: number = currentOffset  - sectionOffset;
            this.feedbackInterval = [feedbackStart, feedbackEnd];
        }
}