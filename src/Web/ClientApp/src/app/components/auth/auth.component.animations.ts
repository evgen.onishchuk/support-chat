import { trigger, transition, style, animate, state, query, group, animateChild, stagger } from "@angular/animations";

export const fade = trigger("fade", [

    state("void", style({opacity: 0})),

    transition(":enter, :leave", [animate(200)])
]);




export const shift = trigger("shift", [

    // state("shifted", style({transform: 'translateX(-50%)', height: '153px'})),
    state("shifted", style({transform: 'translateX(-50%)', height: '153px'})    ),
    // state("normal", style({height: '449px'})),    
    // state("normal", style({height: '*'})),    

    transition("normal => shifted", [
        query(".recoveryForm",style({ opacity:0})),
        animate("0.3s ease-in", style({transform: 'translateX(-50%)'})), 
        animate(300, style({height: '153px'})),
        query(".recoveryForm",animate('1ms 150ms',style({ opacity:1}))),      
        
        
        
    ]),
    transition("shifted => normal", [
        query(".tabset",style({ opacity:0})),
        animate("0.3s ease-in", style({transform: 'translateX(0%)'})),
        animate(300, style({height: '449px'})),
        query(".tabset",animate('1ms 150ms',style({ opacity:1}))),
    ])
]);

export const showError = trigger('showError',[
    state('void',style({ 
        height:0,
        paddingTop: 0,
        paddingBottom: 0,
    })),
    transition(':enter', animate('300ms ease-out', style({
        height:'*',
        paddingTop: '*',
        paddingBottom: '*',
    }))),
    transition(':leave', animate('300ms ease-in'))
]);