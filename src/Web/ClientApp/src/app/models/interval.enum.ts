

export enum Interval {
    None = -1,
    Features,
    Pricing,
    Feedback
}