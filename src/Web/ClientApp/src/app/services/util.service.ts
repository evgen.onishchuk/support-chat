import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Observable";
import { BehaviorSubject } from "rxjs/BehaviorSubject";


@Injectable()
export class UtilService {


    getValueFromObservable<T>(obs:Observable<T>):T{
        if(obs){
            let subject = new BehaviorSubject(null);
            const subscription = obs.subscribe(subject);
            let data:T = subject.getValue();
            subscription.unsubscribe();
            return data;
        }
        return null;
        
    }  

    resolvePromiseFromObservable<T>(obs:Observable<T>):Promise<T>{
        return new Promise((resolve,reject)=> {
            obs.subscribe( value => {
                resolve(value);
            });
        });
    }

}