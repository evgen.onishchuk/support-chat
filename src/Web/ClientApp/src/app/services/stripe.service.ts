import { Injectable } from "@angular/core";
import { Store } from "@ngrx/store";
import { AppState } from "../../store/app.state";
// import { StripeLoadedAction } from "../store/main/main.actions";

declare let Stripe: any;

@Injectable()
export class StripeService {

    private cardNumber: any;

    private stripe: any;

    constructor(private store:Store<AppState>){}

    async initStripeComponents(/*element:HTMLElement*/):Promise<void> {
        const cardNumberDiv: HTMLElement = document.createElement("div");
        this.stripe = Stripe("pk_test_y6ESWK8ghUugPjC5bzybeIoe");
        let elements = this.stripe.elements();
        this.cardNumber = elements.create('cardNumber',{placeholder:""});
        this.cardNumber.on('ready', () => {
            // console.log("Element rendered");
            // this
            // this.store.dispatch(new StripeLoadedAction());
        });        
        this.cardNumber.mount(cardNumberDiv);        
        // console.log("mount started", this.cardNumber);

    }


    get cardNumberElement(): any {
        return this.cardNumber;
    }

}