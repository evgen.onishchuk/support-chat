import { Injectable } from "@angular/core";
import { StripeService } from "../stripe.service";
import { Actions, Effect } from "@ngrx/effects";
import { Action } from "@ngrx/store";
import { Observable } from "rxjs";
import { HidePrebootAction, MainActions } from '../../store/main/main.actions';
import { Router, NavigationStart } from "@angular/router";
import { AuthActions, ShowSignUpAction } from '../../store/auth/auth.actions';


@Injectable()
export class MainEffectsService {

    constructor(private stripeService:StripeService, private actions$: Actions, private router: Router){
        
    }

    // @Effect({dispatch: false})
    // loadStripeElements$: Observable<Action> =
    //      this.actions$.ofType(MainActions.LoadStripeElements)
    //      .do(action => this.stripeService.initStripeComponents());
        // .switchMap( (action: LoadStripeElementsAction)=> {
           
        //     return this.stripeService.initStripeComponents(); })
        // .map(_ => new StripeLoadedAction());

    // @Effect({dispatch: true})
    // loadAccount$: Observable<Action> =  this.router.events.filter( e => {       
    //     return e instanceof NavigationStart && e.url.includes("account")
    // }).do( event => console.log( (event as NavigationStart).url) )
    // .map( (event: NavigationStart) => new ShowPrebootAction(true));

    @Effect({dispatch: false})
    hidePreboot: Observable<Action> = this.actions$.ofType(MainActions.HidePreboot)
        .do( (action: HidePrebootAction) => {
            if(typeof document != 'undefined'){
                window.scrollTo(0,0);
                const body = document.getElementsByTagName("body")[0];
                
                const preboot = document.getElementById("preboot");
                if(action.payload){
                    //  
                    preboot.classList.add("hide");
                    body.classList.remove("hide-scroll");
                    setTimeout(()=>{
                        preboot.classList.add("remove");
                        
                    },800);
                } else {
                    preboot.classList.remove("hide");
                    preboot.classList.remove("remove");
                    body.classList.add("hide-scroll");
                }
            }            
        });
    
    @Effect({dispatch: false})
    hideScrollBelowModal: Observable<Action> = this.actions$.ofType(AuthActions.ShowSignUp)
        .do( (action: ShowSignUpAction) => {
            const body = document.getElementsByTagName("body")[0];
            if(!!action.payload){
                body.classList.add("hide-scroll");
            } else {
                body.classList.remove("hide-scroll");
            }
            
        });
   
    
    
}