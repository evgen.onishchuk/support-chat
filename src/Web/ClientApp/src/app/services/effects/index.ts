import { AuthEffectsService } from "./auth.effects";
import { MainEffectsService } from "./main.effects";

export const MainEffects = [
    AuthEffectsService,
    MainEffectsService
];