import { Action } from '@ngrx/store';
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { Effect, Actions } from '@ngrx/effects';
import { AuthActions, AuthorizeAction, SetTokenAction } from '../../store/auth/auth.actions';
import { AuthService } from '../auth.service';
import { AuthResponse } from '../../models/auth-response';



@Injectable()
export class AuthEffectsService {
    
    constructor(private actions$:Actions, private authService:AuthService){}

    @Effect()
    authorizeUser: Observable<Action> = this.actions$.ofType(AuthActions.Authorize)
        .switchMap( (action: AuthorizeAction)=> {
            return this.authService.authorizeUser(action.payload);
        })
        
        .onErrorResumeNext(Observable.of( { access_token:"" } ))
        
        
        .map((response: AuthResponse) => {            
            return new SetTokenAction(response.access_token)
        }); 

}