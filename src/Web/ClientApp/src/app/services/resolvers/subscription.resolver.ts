import { Injectable } from "@angular/core";
import { Resolve, RouterStateSnapshot, ActivatedRouteSnapshot } from "@angular/router";
import { Store, Action } from "@ngrx/store";
import { AppState } from "../../../store/app.state";
// import { ToggleFooterAction } from "../../store/main/main.actions";
import { Actions } from "@ngrx/effects";
import { MainActions/*, LoadStripeElementsAction*/ } from "../../store/main/main.actions";
import { Observable } from "rxjs/Observable";
import { UtilService } from "../util.service";


@Injectable()
export class SubscriptionResolver implements Resolve<void>{

    constructor(private store:Store<AppState>, private actions$:Actions,private utilService:UtilService){}

    async resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<void> {        
        // this.store.dispatch(new ToggleFooterAction(false));
        // if(typeof window != 'undefined'){
        //     this.store.dispatch(new LoadStripeElementsAction());
        // }  
        
        // console.log("start resolving");
        // await this.stripeLoading();    
    }

    // private async stripeLoading(): Promise<Action> {
    //     const stripeElementsLoading: Observable<Action> = 
    //      this.actions$.ofType(MainActions.StripeLoaded);
    //      console.log("end resolving");
    //      return this.utilService.resolvePromiseFromObservable(stripeElementsLoading);
    // }

}