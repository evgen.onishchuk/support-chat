import { NgModule } from '@angular/core';
import { ServerModule} from '@angular/platform-server';
import { AppModuleShared } from './app.module.shared';
import { MainComponent } from './components/main/main.component';


@NgModule({
    bootstrap: [ MainComponent ],
    imports: [
        ServerModule,
        AppModuleShared
    ]
})
export class AppModule {
}
