import { HomeComponent } from './components/home/home.component';
import { NgModule } from "@angular/core";
import { RouterModule, Routes, PreloadAllModules, ExtraOptions } from "@angular/router";
import { PricingComponent } from './components/pricing/pricing.component';
import { SubscriptionComponent } from './components/subscription/subscription.component';
import { SubscriptionResolver } from './services/resolvers/subscription.resolver';
import { MainComponent } from '../customer/components/main.component';




const routes: Routes = [
    { path: "", component: HomeComponent },
    { path: "pricing", component: PricingComponent },
    { path: "subscription", component: SubscriptionComponent, resolve: { subscription: SubscriptionResolver } },
    { path: "account", loadChildren: "../customer/customer.module#CustomerModule"},
    { path: '**', redirectTo: '' }
];

@NgModule({
    imports: [RouterModule.forRoot(routes, {preloadingStrategy: PreloadAllModules})],
    exports: [RouterModule]
})
export class AppRoutingModule {}