import { Directive, ViewContainerRef, Renderer2, TemplateRef, Input } from '@angular/core';

@Directive({
  selector: '[card-number]',
})
export class CardNumberDirective {
    constructor(public viewContainerRef: ViewContainerRef, private templateRef: TemplateRef<any>, private renderer: Renderer2) { }

    @Input()
    set display(value: boolean) {
        if(value){
            this.viewContainerRef.createEmbeddedView(this.templateRef);
        } else {
            this.viewContainerRef.clear();
        }
       
    }

    // appendComponent(newElement: any) {
        
    //     // this.renderer.appendChild(this.viewContainerRef.element.nativeElement,newElement);  
    // }
}