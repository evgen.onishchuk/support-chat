import { AppRoutingModule } from './app-routing.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { MainComponent } from './components/main/main.component';
import { Ng2PageScrollModule } from 'ng2-page-scroll';
import { AuthComponent } from './components/auth/auth.component';
import { HomeComponent } from './components/home/home.component';
import { PricingComponent } from './components/pricing/pricing.component';
import { NavComponent } from './components/nav/nav.component';
import { AppStoreModule } from '../store/app-store.module';
import { SubscriptionComponent } from './components/subscription/subscription.component';
import { SubscriptionResolver } from './services/resolvers/subscription.resolver';
import { PlansComponent } from './components/subscription/plans/plans.component';
import { CardComponent } from './components/subscription/card/card.component';
import { UtilService } from './services/util.service';
import { StripeService } from './services/stripe.service';
import { CardNumberDirective } from './directives/card-number.directive';
import { FooterComponent } from './components/footer/footer.component';
import { CustomerModule } from '../customer/customer.module';
import { SubscriptionPlansComponent } from './components/home/plans/plans.component';
import { FeaturesComponent } from './components/home/features/features.component';
import { FeadbackComponent } from './components/home/feadback/feadback.component';
import { AuthService } from './services/auth.service';



@NgModule({
    declarations: [ 
        MainComponent,
        AuthComponent,
        HomeComponent,
        PricingComponent,
        NavComponent,
        SubscriptionComponent,
        PlansComponent,
        CardComponent,
        CardNumberDirective,
        FooterComponent,  
        SubscriptionPlansComponent,
        FeaturesComponent,
        FeadbackComponent      
    ],
    imports: [
        CommonModule,
        AppStoreModule,
        HttpClientModule,
        ReactiveFormsModule,
        Ng2PageScrollModule, 
        AppRoutingModule,
       
    ],
    providers: [       
        SubscriptionResolver, UtilService, StripeService, AuthService
    ],
})
export class AppModuleShared {
}
