import { MainState, MAIN_INITIAL_STATE } from "./main.state";
import { Action } from "@ngrx/store";
import { HidePrebootAction, MainActions } from './main.actions';
import { tassign } from "tassign";



export function mainReducer(state: MainState = MAIN_INITIAL_STATE, action: Action): MainState {
    switch(action.type){        
        default: return state;
    }
}

class ReducerActions {
   
}