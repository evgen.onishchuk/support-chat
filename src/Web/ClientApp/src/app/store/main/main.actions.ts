import { Action } from "@ngrx/store";



export enum MainActions {
    HidePreboot = "HidePreboot",
    LoadStripeElements = "LoadStripeElements",
    StripeLoaded = "StripeLoaded"
}

export class HidePrebootAction implements Action {

    readonly type: string = MainActions.HidePreboot;

    constructor(public payload?:boolean){}
}

export class StripeLoadedAction implements Action {
    readonly type: string = MainActions.StripeLoaded;
}

export class LoadStripeElementsAction implements Action {

    readonly type: string = MainActions.LoadStripeElements;

    constructor(public payload?:HTMLElement){}
}