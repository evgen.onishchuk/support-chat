import { AuthState, AUTH_INITIAL_STATE } from "./auth.state";
import { Action } from "@ngrx/store";
import { AuthActions, SetTokenAction } from './auth.actions';

export function authReducer(state: AuthState = AUTH_INITIAL_STATE, action: Action): AuthState {
    switch(action.type){
        case AuthActions.SetToken: ReducerActions.setToken(state,action);
        default: return state;
    }
}


class ReducerActions {

    static setToken(state: AuthState, action: SetTokenAction): AuthState {
        return {
            ...state,
            token: action.payload
        };
    }
}