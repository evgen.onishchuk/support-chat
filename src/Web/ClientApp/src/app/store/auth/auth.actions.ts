import { Action } from "@ngrx/store";

export enum AuthActions {    
    ShowSignUp = "ShowSignUp",
    Authorize = "Authorize",
    SetToken = "SetToken"
}

export class ShowSignUpAction implements Action {

    readonly type:string = AuthActions.ShowSignUp;

    public constructor(public payload?:string){}
}

export class AuthorizeAction implements Action {

    readonly type:string = AuthActions.Authorize;

    public constructor(public payload?: { username:string, password: string}){}
}

export class SetTokenAction implements Action {

    readonly type: string = AuthActions.SetToken;

    constructor(public payload?: string){}
}