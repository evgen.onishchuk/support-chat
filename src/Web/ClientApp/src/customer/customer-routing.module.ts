import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { MainComponent } from "./components/main.component";
import { MainResolver } from "./services/resolvers/main.resolver";

const routes: Routes = [
    { path:"", component: MainComponent, resolve: {main: MainResolver } }
    // { path: "", children: [
    //     { path: "", component: MainComponent, resolve: {main: MainResolver }}
    // ]}
];

@NgModule({
    imports:[RouterModule.forChild(routes)],
    exports:[RouterModule]
})
export class CustomerRoutingModule {
    
}