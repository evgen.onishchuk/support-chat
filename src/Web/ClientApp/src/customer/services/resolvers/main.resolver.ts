import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
// import { ToggleFooterAction } from "../../../app/store/main/main.actions";
import { Actions } from "@ngrx/effects";
import { AppState } from "../../../store/app.state";
import { Store } from "@ngrx/store";

@Injectable()
export class MainResolver implements Resolve<void> {
    
    constructor(private store:Store<AppState>, private actions$:Actions){}

    async resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<void> {        
        // this.store.dispatch(new ToggleFooterAction(false));
    }

}