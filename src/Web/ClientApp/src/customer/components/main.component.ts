import { Component, OnInit, AfterViewInit } from "@angular/core";
import { AppState } from "../../store/app.state";
import { Store } from "@ngrx/store";
import { HidePrebootAction } from "../../app/store/main/main.actions";



@Component({
    selector: "main",
    templateUrl: "./main.component.html",    
    styleUrls: ["./main.component.css"]
})
export class MainComponent implements OnInit, AfterViewInit{

    constructor(private store:Store<AppState>){}


    ngAfterViewInit(): void {        
    }
   
   
    ngOnInit(): void {
    }
    
}