import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { CustomerRoutingModule } from "./customer-routing.module";
import { MainComponent } from "./components/main.component";
import { MainResolver } from "./services/resolvers/main.resolver";



@NgModule({
    declarations:[MainComponent],
    imports:[CommonModule, CustomerRoutingModule],
    providers: [MainResolver],    
})
export class CustomerModule{

}