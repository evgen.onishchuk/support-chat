import { MainState } from "../app/store/main";
import { AuthState } from "../app/store/auth";





export interface AppState{
    main: MainState,
    auth: AuthState
}