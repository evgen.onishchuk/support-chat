import { NgModule } from "@angular/core";
import { StoreModule } from "@ngrx/store";
import { EffectsModule } from "@ngrx/effects";
import { reducers } from "./app.reducers";
import { MainEffects } from "../app/services/effects";

@NgModule({
    imports: [
        StoreModule.forRoot(reducers),
        EffectsModule.forRoot(MainEffects)
    ]
})
export class AppStoreModule{

}