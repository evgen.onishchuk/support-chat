import { ActionReducerMap } from "@ngrx/store";
import { AppState } from "./app.state";
import { mainReducer } from "../app/store/main";
import { authReducer } from '../app/store/auth';

export const reducers: ActionReducerMap<AppState> = {
    main: mainReducer,
    auth: authReducer
}