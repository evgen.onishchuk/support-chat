export enum Interval {
  None = '',
  Body = 'body',
  Features = 'features',
  Pricing = 'pricing',
  Feedback = 'feedback'
}
