import { Action } from '@ngrx/store';
import { Interval } from '../../models/interval.enum';

export enum MainActions {
    HidePreboot = 'HidePreboot',
    SetScrollTarget = 'SetScrollTarget',
    SetCurrentScrollInterval = 'SetCurrentScrollInterval',
    SetPageLoaded = 'SetPageLoaded',
    SetModalOpened = 'SetModalOpened',
    SetScrollOffset = 'SetScrollOffset'
}

export class HidePrebootAction implements Action {

    readonly type: string = MainActions.HidePreboot;

    constructor(public payload?: boolean) {}
}

export class SetScrollTargetAction implements Action {

  readonly type: string = MainActions.SetScrollTarget;

  constructor(public payload: Interval) {}

}

export class SetCurrentScrollIntervalAction implements Action {

  readonly type: string = MainActions.SetCurrentScrollInterval;

  constructor(public payload: Interval) {}
}

export class SetPageLoadedAction implements Action {

  readonly type: string = MainActions.SetPageLoaded;

  constructor(public payload?: boolean) {}
}
export class SetModalOpenedAction implements Action {

  readonly type: string = MainActions.SetModalOpened;

  constructor(public payload?: boolean) {}
}
export class SetScrollOffsetAction implements Action {

  readonly type: string = MainActions.SetScrollOffset;

  constructor(public payload?: number) {}
}



