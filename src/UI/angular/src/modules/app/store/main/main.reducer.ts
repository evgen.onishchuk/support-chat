import { MainState, MAIN_INITIAL_STATE } from './main.state';
import { Action } from '@ngrx/store';
import { MainActions,
  SetScrollTargetAction,
  SetCurrentScrollIntervalAction,
  SetPageLoadedAction,
  SetModalOpenedAction,
  SetScrollOffsetAction
} from './main.actions';



export function mainReducer(state: MainState = MAIN_INITIAL_STATE, action: Action): MainState {

    switch (action.type) {

      case MainActions.SetScrollTarget:
        return ReducerActions.setClickedScroll(state, action as SetScrollTargetAction);

      case MainActions.SetCurrentScrollInterval:
        return ReducerActions.setCurrentScrollInterval( state, action as SetCurrentScrollIntervalAction);

      case MainActions.SetPageLoaded:
        return ReducerActions.setPadeLoaded(state, action);

      case MainActions.SetScrollOffset:
        return ReducerActions.setScrollOffset(state, action);

      default: return state;
    }
  }

class ReducerActions {

  static setClickedScroll(state: MainState, action: SetScrollTargetAction): MainState {
    return {
      ...state,
      scrollTarget: action.payload
    };
  }
  static setCurrentScrollInterval(state: MainState, action: SetCurrentScrollIntervalAction): MainState {

    return {
      ...state,
      currentInterval: action.payload
    };
  }

  static setPadeLoaded(state: MainState, action: SetPageLoadedAction): MainState {

    return  {
      ...state,
      pageLoaded: action.payload
    };
  }

  static setScrollOffset(state: MainState, action: SetScrollOffsetAction): MainState {
        return  {
          ...state,
          scrollOffset: action.payload
        };
  }

}
