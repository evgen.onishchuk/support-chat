import { Injectable } from '@angular/core';
import { Actions, Effect } from '@ngrx/effects';
import { Action, Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import { HidePrebootAction, MainActions } from '../../store/main/main.actions';
import { Router } from '@angular/router';
import { AuthActions, ShowSignUpAction } from '../../store/auth/auth.actions';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/withLatestFrom';
// import 'rxjs/add/operator/delay';
// import 'rxjs/add/observable/empty';
import 'rxjs/add/observable/fromPromise';
// import 'rxjs/add/observable/of';
import { SetPageLoadedAction, SetModalOpenedAction } from './main.actions';
import { AppState } from '../../../store/app.state';


@Injectable()
export class MainEffects {

  constructor(private actions$: Actions, private router: Router, private store: Store<AppState>) {

  }


  @Effect()
  hidePreboot: Observable<Action> = this.actions$.ofType(MainActions.HidePreboot)
    .switchMap((action: HidePrebootAction) => {
      if (typeof document !== 'undefined') {
        window.scrollTo(0, 0);
        const html = document.getElementsByTagName('html')[0];
        html.classList.add('smooth-scroll');

        const body = document.getElementsByTagName('body')[0];
        const preboot = document.getElementById('preboot');

        if (action.payload) {
          preboot.classList.add('hide');
          const promise =  new Promise<Action>( (resolve, reject ) => {
            setTimeout(() => {
              preboot.classList.add('remove');
              resolve(new SetPageLoadedAction(true));
            }, 800);
          });
          return Observable.fromPromise(promise);
        } else {
          preboot.classList.remove('hide');
          preboot.classList.remove('remove');
        }
      }
      return Observable.of({ type: 'EmptyAction' });

    });

  @Effect({ dispatch: false })
  hideScrollBelowModal = this.actions$.ofType(MainActions.SetModalOpened)
    .withLatestFrom(this.store.select(state => state.main.scrollOffset))
    .do(( [action, offset ]: [SetModalOpenedAction, number]) => {
      const html = document.getElementsByTagName('html')[0];
      if (action.payload) {
        html.classList.add('modal-opened');
        document.body.scrollTop = offset;
      } else {
        html.classList.remove('modal-opened');
        html.classList.remove('smooth-scroll');
        window.scrollTo(0, offset);
        html.classList.add('smooth-scroll');
      }

    });

  @Effect({dispatch: false })
  hideScroll = this.actions$.ofType(MainActions.SetPageLoaded)
    .do( (action: SetPageLoadedAction) => {
      if (action.payload) {
        const html = document.getElementsByTagName('html')[0];
        html.classList.remove('modal-opened');
      }
    });
}
