import { Interval } from '../../models/interval.enum';



export interface MainState {
  scrollTarget: Interval;
  currentInterval: Interval;
  pageLoaded: boolean;
  scrollOffset: number;
}

export const MAIN_INITIAL_STATE: MainState = {
  scrollTarget: Interval.None,
  currentInterval: Interval.None,
  pageLoaded: false,
  scrollOffset: 0
};
