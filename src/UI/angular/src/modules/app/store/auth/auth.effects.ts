import { Action } from '@ngrx/store';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Effect, Actions } from '@ngrx/effects';
import { AuthActions, AuthorizeAction, SetTokenAction } from '../../store/auth/auth.actions';
import { AuthService } from '../../services/auth.service';
import { AuthResponse } from '../../models/auth-response';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/onErrorResumeNext';




@Injectable()
export class AuthEffects {

    constructor(private actions$: Actions, private authService: AuthService) {}

    @Effect()
    authorizeUser: Observable<Action> = this.actions$.ofType(AuthActions.Authorize)
        .switchMap( (action: AuthorizeAction) => {
            return this.authService.authorizeUser(action.payload);
        })

        .onErrorResumeNext(Observable.of( { access_token: '' } ))


        .map((response: AuthResponse) => {
            return new SetTokenAction(response.access_token);
        });


    private mapError(error: any): Observable<any> {
      return Observable.of({});
    }

}
