export interface AuthState {
  token: string;
}

export const AUTH_INITIAL_STATE: AuthState = {
  token: ''
};
