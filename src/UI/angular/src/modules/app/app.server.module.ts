import { NgModule } from '@angular/core';
import { ServerModule } from '@angular/platform-server';
import { MainComponent } from './components/main/main.component';
import { AppModule } from './app.module';


@NgModule({

  imports: [
      AppModule,
      ServerModule
  ],
  bootstrap: [MainComponent]

})
export class AppServerModule {}
