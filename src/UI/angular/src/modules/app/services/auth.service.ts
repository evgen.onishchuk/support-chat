import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AuthResponse } from '../models/auth-response';




@Injectable()
export class AuthService {

    constructor(private httpClient: HttpClient) {}

    public async authorizeUser(credentials: {username: string, password: string}): Promise<AuthResponse> {
        return this.httpClient.post<AuthResponse>('http://localhost:3144/login', credentials)
            .toPromise();
    }

}
