import { Component, PLATFORM_ID, OnDestroy, AfterViewInit, Inject, OnInit } from '@angular/core';
import { isPlatformServer } from '@angular/common';
import { Store } from '@ngrx/store';
import { AppState } from '../../../store/app.state';
import { HidePrebootAction } from '../../store/main/main.actions';
import { Observable } from 'rxjs/Observable';
import { Actions } from '@ngrx/effects';
import { AuthActions, ShowSignUpAction } from '../../store/auth';




@Component({
  selector: 'app-root',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit, OnDestroy, AfterViewInit {

  pageDimmed = false;

  ngOnInit(): void {
     this.actions$.ofType(AuthActions.ShowSignUp)
    .map( (action: ShowSignUpAction) => !!action.payload)
    .subscribe ( v => this.pageDimmed = v);

  }

  constructor(@Inject(PLATFORM_ID) private platformId: Object, private store: Store<AppState>, private actions$: Actions) { }

  onActivate() {
    if (!isPlatformServer(this.platformId)) {
      window.scrollTo(0, 0);
    }
  }

  ngAfterViewInit(): void {
    setTimeout(() => {
        this.store.dispatch(new HidePrebootAction(true) );
    }, 1250);

}

  ngOnDestroy(): void {
    this.store.dispatch(new HidePrebootAction(false));
  }

  showScroll(e) {
    console.log(e);
    this.store.dispatch(new ShowSignUpAction());
  }
}
