import { Component, OnInit } from '@angular/core';
import { Actions } from '@ngrx/effects';
import { AuthActions, ShowSignUpAction, AuthorizeAction } from '../../store/auth/auth.actions';
import { AppState } from '../../../store/app.state';
import { Store } from '@ngrx/store';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { AuthService } from '../../services/auth.service';
import { fade, shift, showError } from './auth.component.animations';
import { Observable } from 'rxjs/Observable';
import { SetModalOpenedAction, SetScrollOffsetAction } from '../../store/main/main.actions';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Component({
    selector: 'auth',
    templateUrl: './auth.component.html',
    styleUrls: ['./auth.component.scss'],
    animations: [ fade, shift, showError ]
})
export class AuthComponent implements OnInit {


    showModal: boolean;

    activeTab = 0;

    testError = false;

    focus = '';

    loginStatus = false;



    signinForm: FormGroup;

    recoveryForm: FormGroup = new FormGroup({});

    constructor(private actions$: Actions, private store: Store<AppState>, private authService: AuthService) {}

    ngOnInit(): void {
        this.initLoginForm();
         this.actions$.ofType(AuthActions.ShowSignUp)
            .subscribe((action: ShowSignUpAction) => {
              this.showModal = !!action.payload;
            });

    }

    private initLoginForm(): void {
        this.signinForm = new FormGroup({
            username: new FormControl('', [Validators.required, Validators.email]),
            password: new FormControl('', Validators.required)
        });
    }

    closeModal(): void {
        // this.showModal = false;
        this.store.dispatch(new ShowSignUpAction());
    }

    setActive(activeTab: number): void {
        this.activeTab = activeTab;
    }

    onBlur(): void {
        this.focus = '';
    }

    onFocus(input: string): void {
        this.focus = input;
    }

    async login(): Promise<void> {
        // this.store.dispatch( new AuthorizeAction(this.loginForm.value));
        this.testError = !this.testError;

    }
    hideError(): void {
        this.testError = false;
    }

    showScroll() {
      if (!this.showModal) {

        this.store.dispatch( new SetModalOpenedAction(false));
        this.store.dispatch( new SetScrollOffsetAction( Number.NaN ) );
      }

    }
}
