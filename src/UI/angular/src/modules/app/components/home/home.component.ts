import { Component, ViewChild, AfterViewInit, ElementRef, OnInit, HostListener, EventEmitter } from '@angular/core';
import { NavComponent } from '../nav/nav.component';
import { Interval } from '../../models/interval.enum';
import { FeaturesComponent } from './features/features.component';
import { SubscriptionPlansComponent } from './plans/plans.component';
import { FeadbackComponent } from './feadback/feadback.component';
import { AppState } from '../../../store/app.state';
import { Store } from '@ngrx/store';
import { SetScrollTargetAction, SetCurrentScrollIntervalAction, MainActions, SetScrollOffsetAction } from '../../store/main/main.actions';
import { Actions } from '@ngrx/effects';



@Component({
  selector: 'home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, AfterViewInit {

      @ViewChild(NavComponent)
      navComponent: NavComponent;

      @ViewChild('firstElement')
      firstItem: ElementRef;

      @ViewChild(FeaturesComponent)
      features: FeaturesComponent;

      @ViewChild(SubscriptionPlansComponent)
      pricing: SubscriptionPlansComponent;

      @ViewChild(FeadbackComponent)
      feadback: FeadbackComponent;


      private offset = 75;

      productInterval: [number, number] = [0, 0];

      pricingInterval: [number, number] = [0, 0];

      feedbackInterval: [number, number] = [0, 0];

      activeInterval: Interval = Interval.None;

      invertHeader  = false;

      scrollTarget: Interval;

      targetOffset = 0;

      currentOffset = 0;

      constructor(private store: Store<AppState>, private actions$: Actions ) {}

      ngOnInit(): void {

          this.actions$.ofType(MainActions.SetScrollOffset)
            .subscribe( (action: SetScrollOffsetAction) => {
             return  this.currentOffset = action.payload;
            });

          this.actions$.ofType(MainActions.SetScrollTarget)
            .subscribe( (action: SetScrollTargetAction) => {
              this.scrollTarget = action.payload;
              this.calculateTargetOffset();
              if (this.scrollTarget) {
                this.store.dispatch( new SetCurrentScrollIntervalAction(this.scrollTarget) );
                this.scrollToTarget();
              }
            });

            this.actions$.ofType(MainActions.SetCurrentScrollInterval)
              .subscribe( (action: SetCurrentScrollIntervalAction) => {
                if (this.activeInterval !== action.payload) {
                  this.activeInterval = action.payload;
                }

              });
      }

          ngAfterViewInit(): void {
              this.initOffsetIntervals();
          }

          @HostListener('window:scroll', ['$event'])
          onWindowScroll(event: any): void {
            const value: number =

            (!isNaN(this.currentOffset) ? this.currentOffset : 0) || -document.body.getBoundingClientRect().top;

            if (!this.scrollTarget) {
              if (this.activeInterval !== Interval.Features && this.isInInterval(Interval.Features, value)) {
                this.store.dispatch(new SetCurrentScrollIntervalAction(Interval.Features));
              } else if ( this.activeInterval !== Interval.Pricing && this.isInInterval(Interval.Pricing, value)) {
                this.store.dispatch(new SetCurrentScrollIntervalAction(Interval.Pricing));
              } else if (this.activeInterval !== Interval.Feedback && this.isInInterval(Interval.Feedback, value)) {
                this.store.dispatch(new SetCurrentScrollIntervalAction(Interval.Feedback));
              } else if (this.activeInterval !== Interval.None && this.isInInterval(Interval.None, value)) {
                this.store.dispatch(new SetCurrentScrollIntervalAction(Interval.None));
              }
            } else {
              if (window.pageYOffset === this.targetOffset) {
                this.store.dispatch( new SetScrollTargetAction(Interval.None) );
              }

            }
            this.invertHeader = Math.abs(value) !== 0;
          }


          scrollToTarget(): void {

            window.scrollTo(0, this.targetOffset);

          }

          setActive(value: Interval): void {
              this.activeInterval = value;
          }

          private isInInterval(interval: Interval, value: number): boolean {
              switch (interval) {
                  case Interval.None:
                      return value >= 0 && value <= this.productInterval[0];
                  case Interval.Features:
                      return value >= this.productInterval[0] && value <= this.productInterval[1];
                  case Interval.Pricing:
                      return value >= this.pricingInterval[0] && value <= this.pricingInterval[1];
                  case Interval.Feedback:
                      return value >= this.feedbackInterval[0] && value <= this.feedbackInterval[1];
                  default:
                      return false;
              }
          }

          private initOffsetIntervals(): void {
              const sectionOffset = this.offset + 120;

              let currentOffset: number = this.firstItem.nativeElement.clientHeight;

              const featuresStart: number  = currentOffset  - sectionOffset;
              currentOffset += this.features.nativeElement.clientHeight;
              const featuresEnd: number = currentOffset - sectionOffset;
              this.productInterval = [featuresStart, featuresEnd];

              const pricingStart = currentOffset - sectionOffset;
              currentOffset += this.pricing.nativeElement.clientHeight;
              const pricingEnd = currentOffset  - sectionOffset;
              this.pricingInterval = [pricingStart, pricingEnd];

              const feedbackStart: number = currentOffset - sectionOffset;
              currentOffset += this.feadback.nativeElement.clientHeight;
              const feedbackEnd: number = currentOffset  - sectionOffset;
              this.feedbackInterval = [feedbackStart, feedbackEnd];
          }

          calculateTargetOffset(): void {

            const mainOffset: number = this.firstItem.nativeElement.clientHeight;
            let target = 0;
            switch (this.scrollTarget) {
              case Interval.Features:
                target = mainOffset - this.offset;
                break;
              case Interval.Pricing:
                target = (mainOffset + this.features.nativeElement.clientHeight) - this.offset;
                break;
              case Interval.Feedback:
                target = (mainOffset + this.features.nativeElement.clientHeight + this.pricing.nativeElement.clientHeight) - this.offset;
                break;

            }
            this.targetOffset = target;
          }
  }
