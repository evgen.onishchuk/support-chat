import { Observable } from 'rxjs/Observable';
import { Component,
  HostListener,
  ElementRef,
  ViewChild,
  EventEmitter,
  Output,
  OnInit,
  Input,
  AfterViewInit,
  AfterViewChecked } from '@angular/core';
import { Interval } from '../../models/interval.enum';
import { Store } from '@ngrx/store';
import { AppState } from '../../../store/app.state';
import { ShowSignUpAction, AuthActions } from '../../store/auth';
import { Actions } from '@ngrx/effects';
import { SetScrollTargetAction, MainActions, SetModalOpenedAction, SetScrollOffsetAction } from '../../store/main/main.actions';


@Component({
    selector: 'app-nav',
    templateUrl: './nav.component.html',
    styleUrls: [ './nav.component.scss' ]
})
export class NavComponent implements OnInit, AfterViewInit, AfterViewChecked {


    @ViewChild('header')
    header: ElementRef;

    @Input('invertHeader')
    invertHeader = true;

    @Input('mainHeader')
    mainHeader = false;

    displayPopup = false;

    modalOpened$: Observable<boolean>;
    pageLoaded$: Observable<boolean>;

    currentInterval$: Observable<Interval>;

    constructor(private store: Store<AppState>, private actions$: Actions) {}

    ngOnInit(): void {

        this.modalOpened$ = this.actions$.ofType(MainActions.SetModalOpened)
            .map( (action: SetModalOpenedAction) => {
              return action.payload;
            });



        this.currentInterval$ =
          this.store.select( (state: AppState) =>  state.main.currentInterval );
          this.pageLoaded$ = this.store.select( (state: AppState ) => {
            return state.main.pageLoaded;
           });
    }

    ngAfterViewInit(): void {
    }

    ngAfterViewChecked(): void {
    }


    @HostListener('body:click', ['$event'])
    onBodyClick(event: any) {
        this.displayPopup = false;
    }

    showAuth(authType: string): void {
      this.store.dispatch(new SetScrollOffsetAction( window.scrollY ));
      this.store.dispatch(new ShowSignUpAction(authType));
      this.store.dispatch(new SetModalOpenedAction(true));
    }

    isAuthorized(): boolean {
        return false;
    }

    showPopup(): void {
        this.displayPopup = !this.displayPopup;
    }

    scrollTo(target: Interval) {
      this.store.dispatch(new SetScrollTargetAction(target));
    }
}
