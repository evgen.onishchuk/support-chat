import { NgModule, Inject, PLATFORM_ID } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { MainComponent } from './components/main/main.component';
import { AppRoutingModule } from './app-routing.module';
import { HomeComponent } from './components/home/home.component';
import { NavComponent } from './components/nav/nav.component';
import { AppStoreModule } from '../store/app-store.module';
import { FeaturesComponent } from './components/home/features/features.component';
import { SubscriptionPlansComponent } from './components/home/plans/plans.component';
import { FeadbackComponent } from './components/home/feadback/feadback.component';
import { ReactiveFormsModule } from '@angular/forms';
import { AuthService } from './services/auth.service';
import { HttpClientModule } from '@angular/common/http';
import { FooterComponent } from './components/footer/footer.component';
import { SuiDimmerModule } from 'ng2-semantic-ui';
import { AuthComponent } from './components/auth/auth.component';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { isPlatformServer } from '@angular/common';


@NgModule({
    declarations: [
      MainComponent,
      HomeComponent,
      NavComponent,
      FeaturesComponent,
      SubscriptionPlansComponent,
      FeadbackComponent,
      FooterComponent,
      AuthComponent

    ],
    imports: [
        BrowserModule.withServerTransition({ appId: 'supportchat' }),
        AppRoutingModule,
        AppStoreModule,
        ReactiveFormsModule,
        HttpClientModule,
        // SuiDimmerModule,
        StoreDevtoolsModule.instrument({
          maxAge: 25 //  Retains last 25 states
        }),
        ReactiveFormsModule,
        BrowserAnimationsModule
    ],
    providers: [ AuthService ],
    bootstrap: [MainComponent]

})
export class AppModule {

  // constructor(@Inject(PLATFORM_ID) private platformId: Object) {}

  // public getSuiModules(): any {
  //   if (!isPlatformServer(this.platformId)) {
  //     return SuiDimmerModule;
  //   } else {
  //     return [];
  //   }
  // }
}
