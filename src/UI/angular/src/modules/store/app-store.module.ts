import { NgModule } from '@angular/core';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { reducers } from './app.reducers';
import { MainEffects } from '../app/store/main';
import { AuthEffects } from '../app/store/auth';

@NgModule({
    imports: [
        StoreModule.forRoot(reducers),
        EffectsModule.forRoot([ MainEffects, AuthEffects])
    ]
})
export class AppStoreModule {

}
