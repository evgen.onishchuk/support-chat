﻿using static Identity.Config.IdentityServer.IdentityConfig;
using IdentityServer4.EntityFramework.DbContexts;
using IdentityServer4.EntityFramework.Mappers;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Identity.Database.Seed
{
    public class ConfigurationDbContextSeed
    {
        public async Task SeedAsync(ConfigurationDbContext context, IServiceProvider serviceProvider)
        {
            if (!context.Clients.Any())
            {
                foreach (var client in Clients)
                {
                    await context.Clients.AddAsync(client.ToEntity());
                }                
            }

            if (!context.IdentityResources.Any())
            {
                foreach (var resource in IdentityResources)
                {
                    await context.IdentityResources.AddAsync(resource.ToEntity());
                }               
            }

            if (!context.ApiResources.Any())
            {
                foreach (var api in ApiResources)
                {
                    await context.ApiResources.AddAsync(api.ToEntity());
                }
               
            }

            await context.SaveChangesAsync();
        }
    }
}
