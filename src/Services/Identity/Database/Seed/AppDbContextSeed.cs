﻿using Identity.Database.Contexts.Identity;
using Identity.Database.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Identity.Database.Seed
{
    public class AppDbContextSeed
    {
        public async Task SeedAsync (AppDbContext context, IServiceProvider serviceProvider)
        {
            var userManager = serviceProvider.GetRequiredService<UserManager<AppUser>>();
            var roleManager = serviceProvider.GetRequiredService<RoleManager<IdentityRole>>();

            if(!context.Roles.Any())
            {                
                var roles = Enum.GetNames(typeof(UserRoles));

                foreach(var role in roles)
                {
                    await roleManager.CreateAsync(new IdentityRole(role));
                }
                
            }

            if(!context.Users.Any())
            {
                string user = "customer@gmail.com";
                string password = "aS19021989*";
                var result = await userManager.CreateAsync(new AppUser { UserName = user, Email = user }, password);              
                await userManager.AddToRoleAsync(await userManager.FindByNameAsync(user), nameof(UserRoles.Customer));
            }
        }
    }
}
