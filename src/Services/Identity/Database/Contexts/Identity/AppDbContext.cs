﻿using Identity.Database.Entities;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Protocols;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Identity.Database.Contexts.Identity
{
    public class AppDbContext:IdentityDbContext<AppUser>
    {  
        //public AppDbContext()
        //{
            
        //}

        public AppDbContext(DbContextOptions<AppDbContext> options): base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);           
        }

        //protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        //{            
        //    optionsBuilder.UseMySql(Startup.Configuration.GetConnectionString("IdentityConnection"));
        //    base.OnConfiguring(optionsBuilder);
        //}
    }
}
