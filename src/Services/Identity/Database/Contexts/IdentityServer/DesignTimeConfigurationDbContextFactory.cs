﻿using IdentityServer4.EntityFramework.DbContexts;
using IdentityServer4.EntityFramework.Options;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using static Identity.Extensions.ServicesCollectionExtensions;

namespace Identity.Database.Contexts.IdentityServer
{
    public class DesignTimeConfigurationDbContextFactory : IDesignTimeDbContextFactory<IdentityConfigurationDbContext>
    {
        public IdentityConfigurationDbContext CreateDbContext(string[] args)
        {            
            var builder = new DbContextOptionsBuilder<ConfigurationDbContext>()
                .UseMySql(IdentityServerConnectionString);

            return new IdentityConfigurationDbContext(builder.Options, new ConfigurationStoreOptions
            {
                ConfigureDbContext = StoreOptions
            });
        }
    }
}
