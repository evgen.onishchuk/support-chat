﻿using IdentityServer4.EntityFramework.DbContexts;
using IdentityServer4.EntityFramework.Options;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using static Identity.Extensions.ServicesCollectionExtensions;


namespace Identity.Database.Contexts.IdentityServer
{
    public class DesignTimePersistedGrantDbContextFactory : IDesignTimeDbContextFactory<IdentityPersistedGrantDbContext>
    {
        public IdentityPersistedGrantDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<PersistedGrantDbContext>()           
                .UseMySql(IdentityServerConnectionString);

            return new IdentityPersistedGrantDbContext(builder.Options, new OperationalStoreOptions
            {
                ConfigureDbContext = StoreOptions
            });
        }
    }
}
