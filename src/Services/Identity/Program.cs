﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using WebHostExtensions;
using Identity.Database.Seed;
using Identity.Database.Contexts.Identity;
using Identity.Database.Contexts.IdentityServer;

namespace Identity
{
    public class Program
    {
        public static void Main(string[] args)
        {
            BuildWebHost(args)
                .MigrateDbContext<IdentityPersistedGrantDbContext>()

                .MigrateDbContext<IdentityConfigurationDbContext>((context, services) =>
                    new ConfigurationDbContextSeed()
                        .SeedAsync(context, services)
                        .Wait())

                 .MigrateDbContext<AppDbContext>((context, services) => 
                    new AppDbContextSeed()
                    .SeedAsync(context,services)
                    .Wait())
                .Run();
        }

        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)            
                .UseStartup<Startup>()
                .Build();       
    }
}
