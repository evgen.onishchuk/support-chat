﻿using Identity.Database.Contexts.Identity;
using Identity.Database.Contexts.IdentityServer;
using Identity.Database.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Reflection;
using System.Security.Cryptography.X509Certificates;
using static System.IO.Path;

namespace Identity.Extensions
{
    public static class ServicesCollectionExtensions
    {

        public static string IdentityServerConnectionString => Startup.Configuration.GetConnectionString("IdentityServerConnection");

        private static string migrationsAssembly = typeof(Startup).GetTypeInfo().Assembly.GetName().Name;

        public static Action<DbContextOptionsBuilder> StoreOptions =>
            builder => builder.UseMySql(
                    IdentityServerConnectionString,
                    opts => opts.MigrationsAssembly(migrationsAssembly)
                );





        public static IServiceCollection AddApplicationDbContext(this IServiceCollection services)
        {
            services.AddDbContext<AppDbContext>(options => options.UseMySql(Startup.Configuration.GetConnectionString("IdentityConnection")));

            return services;
        }

        public static IServiceCollection AddIdentityServerCustomContexts(this IServiceCollection services)
        {
            services.AddDbContext<IdentityConfigurationDbContext>(options => options.UseMySql(IdentityServerConnectionString));
            services.AddDbContext<IdentityPersistedGrantDbContext>(options => options.UseMySql(IdentityServerConnectionString));

            return services;
        }

        public static IServiceCollection AddApplicationIdentity(this IServiceCollection services)
        {
            services.AddIdentity<AppUser, IdentityRole>()
                .AddEntityFrameworkStores<AppDbContext>()
                .AddDefaultTokenProviders();

            return services;
        }

        public static IServiceCollection AddApplicationIdentityServer(this IServiceCollection services)
        {            
            var certificate = new X509Certificate2(Combine(Startup.Environment.ContentRootPath, "Certificate", "identity.pfx"), "123456");   

            services.AddIdentityServer()
                .AddSigningCredential(certificate)
                .AddConfigurationStore(options =>
                {
                    options.ConfigureDbContext = StoreOptions;
                })
                .AddOperationalStore(options =>
                {
                    options.ConfigureDbContext = StoreOptions;
                })
                //.AddResourceOwnerValidator<ResourceOwnerPasswordValidator>()
                .AddAspNetIdentity<AppUser>();
                //.AddProfileService<IdentityProfileService>(); // adding custom claims to token

            return services;
        }
    }
}
