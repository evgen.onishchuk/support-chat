﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Gateway.Config
{
    public class Authorization
    {
        public string Server { get; set; }
        public string ClientId { get; set; }
        public string ClientSecret { get; set; }
    }
}
