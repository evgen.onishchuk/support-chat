﻿using Gateway.Config;
using IdentityModel.Client;
using Microsoft.Extensions.Options;
using Newtonsoft.Json.Linq;
using System.Threading.Tasks;

namespace Gateway.Services
{
    public class AuthService : IAuthService
    {    
        public Authorization AuthConfig { get; }

        public AuthService(IOptions<Authorization> authoptions)
        {
            AuthConfig = authoptions.Value;
        }

        public async Task<JObject> GetTokenAsync(string username, string password)
        {           
            var client = new TokenClient(
                $"{AuthConfig.Server}/connect/token",
                AuthConfig.ClientId,
                AuthConfig.ClientSecret);
            //await client.RequestClientCredentialsAsync(); // for social login 
            return (await client.RequestResourceOwnerPasswordAsync(username, password)).Json; 
        }
    }
}
