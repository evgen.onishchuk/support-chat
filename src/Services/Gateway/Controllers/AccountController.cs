﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Gateway.Config;
using Gateway.Services;
using Gateway.ViewModel;
using Newtonsoft.Json.Linq;

namespace Gateway.Controllers
{
    [Produces("application/json")]    
    public class AccountController : Controller
    {

        public IAuthService AuthService { get; }

        public AccountController(IAuthService authService)
        {
            AuthService = authService;
        }

        [HttpGet("/externallogin")]
        public IActionResult Index() => Redirect("http://localhost:6955/externallogin");


        [HttpPost("/login")]
        public async Task<IActionResult> Login([FromBody] LoginViewModel userCredentials)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            var (username, password) = userCredentials;

            JObject response = await AuthService.GetTokenAsync(username, password);
            var error = response.GetValue("error");
            //bool requestFailed = response.GetValue("error").Type != JTokenType.Null;

            if (error!=  null)
            {
                return BadRequest(response);
            }
            return Ok(response);
        }
    }
}