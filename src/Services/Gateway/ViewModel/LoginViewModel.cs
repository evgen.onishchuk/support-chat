﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Gateway.ViewModel
{
    public class LoginViewModel
    {
        public string Username { get; set; }
        public string Password { get; set; }

        public void Deconstruct(out string username, out string password)
            => (username, password) = (Username, Password);
       
    }
}
